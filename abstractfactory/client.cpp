#include "client.h"
#include "concrete_factory1.h"
#include "concrete_factory2.h"

Client::Client(void) : m_factory(new ConcreteFactory2())
{
	if (m_factory)
	{
		m_factory->CreateProductA();
		m_factory->CreateProductB();
	}
}


Client::~Client(void)
{
	if (m_factory)
	{
		delete m_factory;
		m_factory = nullptr;
	}
}
