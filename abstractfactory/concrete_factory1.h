#ifndef CONCRETE_FACTORY_H
#define CONCRETE_FACTORY_H
#include "abstract_factory.h"
class ConcreteFactory1 : public AbstractFactory {
public:
	AbstractProductA* CreateProductA() override;
	AbstractProductB* CreateProductB() override;
};
#endif//CONCRETE_FACTORY_H
