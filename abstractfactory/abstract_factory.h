#ifndef ABSTRACT_FACTORY_H
#define ABSTRACT_FACTORY_H
/*
*提供一个创建一系列相关或相互依赖对象的接口，而无需指定它们具体的类(使用抽象的基类来接收这些对象)。
*工厂对象
*/
class AbstractProductA;
class AbstractProductB;
class AbstractFactory {
public:
	virtual AbstractProductA* CreateProductA() = 0;
	virtual AbstractProductB* CreateProductB() = 0;
};
#endif//ABSTRACT_FACTORY_H
