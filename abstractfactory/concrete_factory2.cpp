#include "concrete_factory2.h"
#include "producta2.h"
#include "productb2.h"

ConcreteFactory2::ConcreteFactory2(void)
{
}


ConcreteFactory2::~ConcreteFactory2(void)
{
}

AbstractProductA* ConcreteFactory2::CreateProductA()
{
	return new ProductA2();
}

AbstractProductB* ConcreteFactory2::CreateProductB()
{
	return new ProductB2();
}
