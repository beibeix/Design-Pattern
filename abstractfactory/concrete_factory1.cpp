#include "concrete_factory1.h"
#include "producta1.h"
#include "productb1.h"

AbstractProductA* ConcreteFactory1::CreateProductA()
{
	AbstractProductA* rt = new ProductA1();
	return rt;
}

AbstractProductB* ConcreteFactory1::CreateProductB()
{
	AbstractProductB* rt = new ProductB1();
	return rt;
}
