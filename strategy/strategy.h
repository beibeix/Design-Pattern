#ifndef STRATEGY_H
#define STRATEGY_H

class ArithmeticOperator{
public:
	virtual int operate(int, int) = 0;
};

class AddOperator : public ArithmeticOperator {
private:
	int operate(int left, int right) override;
};

class SubOperator : public ArithmeticOperator {
private:
	int operate(int left, int right) override;
};

#endif//STRATEGY_H
