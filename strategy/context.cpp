#include "context.h"

Context::Context(ArithmeticOperator* operate) 
	:m_arithmetic_operator(operate)
{
	 
}

int Context::Calculate(int left, int right)
{
	return m_arithmetic_operator->operate(left, right);
}
