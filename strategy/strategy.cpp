#include "strategy.h"

int AddOperator::operate(int left, int right)
{
	return left + right;
}

int SubOperator::operate(int left, int right)
{
	return left - right;
}
