#ifndef CONTEXT_H
#define CONTEXT_H
#include "strategy.h"

class Context{
public:
	Context(ArithmeticOperator* operate);
	int Calculate(int left, int right);

private:
	ArithmeticOperator *m_arithmetic_operator;
};
#endif//CONTEXT_H
