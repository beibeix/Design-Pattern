#include "Prototype.h"
#include "iostream"
using namespace std;

Prototype::Prototype() 
{
	cout<<"Prototype"<<endl;
}

Prototype::~Prototype()
{
	cout<<"~Prototype"<<endl;
}

//ConcretePrototype1
ConcretePrototype1::ConcretePrototype1()
{
	cout<<"ConcretePrototype1"<<endl;
}

ConcretePrototype1::~ConcretePrototype1()
{
	cout<<"~ConcretePrototype1"<<endl;
}

ConcretePrototype1::ConcretePrototype1(const ConcretePrototype1& cp)
{
	cout<<"ConcretePrototype1 copy"<<endl;
}

ConcretePrototype1* ConcretePrototype1::Clone() const
{
	//用原型实例*this创建新的对象.OpenCV中的Mat类就是用这种方式实现的
	return new ConcretePrototype1(*this);
}

//ConcretePrototype2
ConcretePrototype2::ConcretePrototype2()
{
	cout<<"ConcretePrototype2"<<endl;
}

ConcretePrototype2::~ConcretePrototype2()
{
	cout<<"~ConcretePrototype2"<<endl;
}

ConcretePrototype2::ConcretePrototype2(const ConcretePrototype2& cp)
{
	cout<<"ConcretePrototype2 copy"<<endl;
}

ConcretePrototype2* ConcretePrototype2::Clone() const
{
	return new ConcretePrototype2(*this);
}