#ifndef CONCRETE_MEDIATOR_H
#define CONCRETE_MEDIATOR_H

#include "mediator.h"
#include "concrete_colleague1.h"
#include "concrete_colleague2.h"

class ConcreteMediator :
	public Mediator
{
public:
	ConcreteMediator(void);
	~ConcreteMediator(void);

private:
	ConcreteColleague1 m_colleague1;
	ConcreteColleague2 m_colleague2;
};

#endif//CONCRETE_MEDIATOR_H