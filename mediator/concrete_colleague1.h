#ifndef CONCRETE_COLLEAGUE_H
#define CONCRETE_COLLEAGUE_H

#include "colleague.h"
class ConcreteColleague1 :
	public Colleague
{
public:
	ConcreteColleague1(Mediator *mediator);
	~ConcreteColleague1(void);
};

#endif//CONCRETE_COLLEAGUE_H