#ifndef COLLEAGUE_H
#define COLLEAGUE_H

class Mediator;
class Colleague {
public: 
	Colleague(Mediator *mediator) : m_mediator(mediator) {}

protected:
	Mediator *m_mediator;
};


#endif//COLLEAGUE_H
