#include "flyweight_factory.h"
#include "concrete_flyweight.h"

FlyweightFactory::FlyweightFactory(void)
	:m_flyweights(std::map<std::string,Flyweight*>())
{
	m_flyweights.clear();
}


FlyweightFactory::~FlyweightFactory(void)
{
	for (Iter itr = m_flyweights.begin(); itr != m_flyweights.end(); ++itr)
	{
		if (itr->second)
		{
			delete itr->second;
			itr->second = nullptr;
		}
	}
	m_flyweights.clear();
}

Flyweight* FlyweightFactory::GetFlyweight(const std::string &key)
{
	if (!IsContain(key))//如果该对象尚未创建,创建该对象.
	{
		m_flyweights[key] = new ConcreteFlyweight;
	} 
	
	return m_flyweights[key];
}

bool FlyweightFactory::IsContain(const std::string &key) const
{
	for (CItr itr = m_flyweights.cbegin(); itr != m_flyweights.cend(); ++itr)
	{
		if (0 == key.compare(itr->first))
		{
			return true;
		} 
		else
		{
			return false;
		}
	}
    return false;
}
