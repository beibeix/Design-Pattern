#ifndef UNSHARD_CONCRETE_FLYWEIGHT_H
#define UNSHARD_CONCRETE_FLYWEIGHT_H

#include "flyweight.h"
class UnsharedConcreteFlyweight :
	public Flyweight
{
public:
	UnsharedConcreteFlyweight(void);
	~UnsharedConcreteFlyweight(void);
	void Operation(ExtrinsicState &ex_state) override;
};

#endif//UNSHARD_CONCRETE_FLYWEIGHT_H