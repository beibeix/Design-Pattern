#ifndef FLYWEIGHT_FACTORY_H
#define FLYWEIGHT_FACTORY_H

#include <map>
#include <string>
#include "flyweight.h"

class FlyweightFactory
{
	typedef std::map<std::string,Flyweight*>::const_iterator CItr;
	typedef std::map<std::string,Flyweight*>::iterator Iter;
public:
	FlyweightFactory(void);
	~FlyweightFactory(void);
	Flyweight* GetFlyweight(const std::string &key);

private:
	bool IsContain(const std::string &key) const;

private:
	std::map<std::string,Flyweight*> m_flyweights;
};

#endif//FLYWEIGHT_FACTORY_H