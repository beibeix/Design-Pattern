#ifndef CONCRETE_FLYWEIGHT_H
#define CONCRETE_FLYWEIGHT_H

#include "flyweight.h"
#include "intrinsic_state.h"

class ConcreteFlyweight :
	public Flyweight
{
public:
	ConcreteFlyweight(void);
	~ConcreteFlyweight(void);
	void Operation(ExtrinsicState &ex_state) override;

private:
	IntrinsicState m_in_state;
};

#endif//CONCRETE_FLYWEIGHT_H