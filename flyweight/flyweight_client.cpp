#include "flyweight_client.h"
#include "flyweight_factory.h"
#include "extrinsic_state.h"

FlyweightClient::FlyweightClient(void)
{
	FlyweightFactory fwf;
	fwf.GetFlyweight(std::string("123"));
	fwf.GetFlyweight(std::string("456"));

	Flyweight *fw = fwf.GetFlyweight(std::string("123"));
	ExtrinsicState es;
	fw->Operation(es);
}


FlyweightClient::~FlyweightClient(void)
{
}
