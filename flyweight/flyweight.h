#ifndef FLYWEIGHT_H
#define FLYWEIGHT_H
/************************************************************************/
/*    运用共享技术有效地支持大量细粒度的对象                               */
/************************************************************************/

class ExtrinsicState;
class Flyweight
{
public:
	virtual void Operation(ExtrinsicState&) = 0;
protected:
private:
};

#endif//FLYWEIGHT_H
