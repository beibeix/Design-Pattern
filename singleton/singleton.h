#ifndef SINGLETON_H
#define SINGLETON_H

#include <iostream>
/************************************************************************/
/* 单件模式                                                             */
/* 保证一个类仅有一个实例，并提供一个访问它的全局访问点。                   */
/************************************************************************/
//线程不安全
class Singleton {
public:
	static Singleton* GetInstance() 
	{
		if (!m_instance)
			m_instance = new Singleton();
		return m_instance;
	}

private:
	Singleton() { std::cout << " singleton " << std::endl; }
	class Collector {
	public:
        Collector() {}
		~Collector()
		{
			if (Singleton::m_instance)
			{
				delete Singleton::m_instance;
				Singleton::m_instance = nullptr;
			}
		}
	};
	static Collector collector;

private:
	static Singleton* m_instance;
};

Singleton* Singleton::m_instance = nullptr;


//线程安全
class ESingleton {
public:
	ESingleton* GetInstance()
	{
		static ESingleton instance;
		return &instance;
	}

private:
	ESingleton()
	{
		//TODO:
	}
};

#endif//SINGLETON_H
