#ifndef CONCRETE_VISITOR_1_H
#define CONCRETE_VISITOR_1_H

#include "visitor.h"
class ConcreteVisitor1 :
	public Visitor
{
public:
	ConcreteVisitor1(void);
	~ConcreteVisitor1(void);
	void VisitConcreteElementA(ConcreteElementA *element_a) override;
	void VisitConcreteElementB(ConcreteElementB *element_b) override;
};

#endif//CONCRETE_VISITOR_1_H