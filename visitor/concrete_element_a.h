#ifndef CONCRETE_ELEMENT_A_H
#define CONCRETE_ELEMENT_A_H

#include "element.h"

class ConcreteElementA :
	public Element
{
public:
	ConcreteElementA(void);
	~ConcreteElementA(void);
	void Accept(Visitor* v) override;
	void OperationA();
};

#endif//CONCRETE_ELEMENT_A_H