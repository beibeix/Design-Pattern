#include "concrete_visitor_2.h"
#include "concrete_element_a.h"
#include "concrete_element_b.h"

ConcreteVisitor2::ConcreteVisitor2(void)
{
}


ConcreteVisitor2::~ConcreteVisitor2(void)
{
}

void ConcreteVisitor2::VisitConcreteElementA(ConcreteElementA *element_a)
{
	//定义作用于元素的新操作
	element_a->OperationA();
}

void ConcreteVisitor2::VisitConcreteElementB(ConcreteElementB *element_b)
{
	//定义作用于元素的新操作
	element_b->OperationB();

}
