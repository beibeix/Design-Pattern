#include "concrete_visitor_1.h"
#include "concrete_element_a.h"
#include "concrete_element_b.h"

ConcreteVisitor1::ConcreteVisitor1(void)
{
}


ConcreteVisitor1::~ConcreteVisitor1(void)
{
}

void ConcreteVisitor1::VisitConcreteElementA(ConcreteElementA *element_a)
{
	//定义作用于元素的新操作
	element_a->OperationA();
}

void ConcreteVisitor1::VisitConcreteElementB(ConcreteElementB *element_b)
{
	//定义作用于元素的新操作
	element_b->OperationB();
}
