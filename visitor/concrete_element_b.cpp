#include "concrete_element_b.h"
#include "visitor.h"

ConcreteElementB::ConcreteElementB(void)
{
}


ConcreteElementB::~ConcreteElementB(void)
{
}

void ConcreteElementB::Accept(Visitor* v)
{
	v->VisitConcreteElementB(this);
}

void ConcreteElementB::OperationB()
{

}
