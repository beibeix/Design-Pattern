#ifndef CONCRETE_VISITOR_2_H
#define CONCRETE_VISITOR_2_H

#include "visitor.h"

class ConcreteVisitor2 :
	public Visitor
{
public:
	ConcreteVisitor2(void);
	~ConcreteVisitor2(void);
	void VisitConcreteElementA(ConcreteElementA *element_a) override;
	void VisitConcreteElementB(ConcreteElementB *element_b) override;
};

#endif//CONCRETE_VISITOR_2_H