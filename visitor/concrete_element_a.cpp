#include "concrete_element_a.h"
#include "visitor.h"

ConcreteElementA::ConcreteElementA(void)
{
}


ConcreteElementA::~ConcreteElementA(void)
{
}

void ConcreteElementA::Accept(Visitor* v)
{
	v->VisitConcreteElementA(this);
}

void ConcreteElementA::OperationA()
{

}
