#ifndef OBJECT_STRUCT_H
#define OBJECT_STRUCT_H

#include <vector>

class Element;
class Visitor;

class ObjectStruct
{
public:
	ObjectStruct();
	~ObjectStruct(void);

	void AddElement(Element *e);
	void Act(Visitor *v);

private:
	std::vector<Element*> m_elements;
	
};

#endif//OBJECT_STRUCT_H