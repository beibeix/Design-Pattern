#ifndef VISITOR_H
#define VISITOR_H

class ConcreteElementA;
class ConcreteElementB;

class Visitor
{
public:
	virtual void VisitConcreteElementA(ConcreteElementA*) = 0;
	virtual void VisitConcreteElementB(ConcreteElementB*) = 0;

protected:
private:
};


#endif//VISITOR_H
