#ifndef CONCRETE_ELEMENT_B_H
#define CONCRETE_ELEMENT_B_H

#include "element.h"

class ConcreteElementB :
	public Element
{
public:
	ConcreteElementB(void);
	~ConcreteElementB(void);

	void Accept(Visitor* v) override;
	void OperationB();
};

#endif//CONCRETE_ELEMENT_B_H