#include "visitor_client.h"
#include "object_struct.h"
#include "concrete_visitor_1.h"
#include "concrete_element_a.h"
#include "concrete_element_b.h"

VisitorClient::VisitorClient(void)
{
	//construct a concrete visitor object.
	ConcreteVisitor1 cv1;
	//construct a object_struct object.
	ObjectStruct os;
	//add element into object_struct.
	os.AddElement(new ConcreteElementA);
	os.AddElement(new ConcreteElementB);

	os.Act(&cv1);
}


VisitorClient::~VisitorClient(void)
{
}
