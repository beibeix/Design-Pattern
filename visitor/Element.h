#ifndef ELEMENT_H
#define ELEMENT_H

class Visitor;
class Element
{
public:
	virtual void Accept(Visitor*) = 0;

protected:
private:
};

#endif//ELEMENT_H
