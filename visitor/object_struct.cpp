#include "object_struct.h"
#include "Element.h"


ObjectStruct::ObjectStruct() : 
	m_elements(std::vector<Element*>())
{
}


ObjectStruct::~ObjectStruct(void)
{
	for (auto p : m_elements)
	{
		if (p)
		{
			delete p;
			p = nullptr;
		}
	}
	m_elements.clear();
}

void ObjectStruct::AddElement(Element *e)
{
	m_elements.push_back(e);
}

void ObjectStruct::Act(Visitor *v)
{
	for (auto p : m_elements)
	{
		if (p)
		{
			p->Accept(v);
		}
	}
}
