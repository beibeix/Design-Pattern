#ifndef SUBJECT_H
#define SUBJECT_H

#include <vector>
class Observer;
class SubjectState;
class Subject
{
public:
	Subject();
	virtual ~Subject();	
	void Attach(Observer *o);
	void Detach(Observer *o);
	void Notify();
	virtual SubjectState GetState() const = 0;
protected:
	std::vector<Observer*> m_observers;
};

#endif//SUBJECT_H
