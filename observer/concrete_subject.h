#ifndef CONCRETE_SUBJECT_H
#define CONCRETE_SUBJECT_H

#include "subject.h"
#include "subject_state.h"

class ConcreteSubject :
	public Subject
{
public:
	ConcreteSubject(void);
	~ConcreteSubject(void);
	void SetState(const SubjectState &state);
	SubjectState GetState() const override;
private:
	SubjectState m_state;
};

#endif//CONCRETE_SUBJECT_H