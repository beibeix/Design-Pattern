#include "observer_client.h"
#include "concrete_observer.h"
#include "concrete_subject.h"

ObserverClient::ObserverClient(void)
{
	ConcreteSubject cs;//目标对象
	ConcreteObserver co1(&cs);//观察者对象
	ConcreteObserver co2(&cs);//观察者对象
	cs.Attach(&co1);//观察者对象关注目标
	cs.Attach(&co2);//观察者对象关注目标
	cs.Notify();//目标更新,通知观察者更新
	cs.Detach(&co2);
	cs.Notify();
}


ObserverClient::~ObserverClient(void)
{
}
