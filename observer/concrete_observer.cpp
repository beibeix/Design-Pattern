#include "concrete_observer.h"
#include "subject.h"
#include "subject_state.h"

ConcreteObserver::ConcreteObserver(Subject *s)
	: Observer(s) {

}

ConcreteObserver::~ConcreteObserver(void)
{
}

void ConcreteObserver::Update() {
	m_subject->GetState();
}
