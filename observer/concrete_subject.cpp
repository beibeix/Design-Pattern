#include "concrete_subject.h"

ConcreteSubject::ConcreteSubject(void) {
}


ConcreteSubject::~ConcreteSubject(void) {
}

void ConcreteSubject::SetState(const SubjectState &state) {
	m_state = state;
}

SubjectState ConcreteSubject::GetState() const {
	return m_state;
}
