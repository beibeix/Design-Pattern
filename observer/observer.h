#ifndef OBSERVER_H
#define OBSERVER_H

class Subject;
class Observer
{
public:
	explicit Observer(Subject *s);
	virtual ~Observer(void);
	virtual void Update();//SUPPORT EXTENSION.

protected:
	Subject *m_subject;
};

#endif//OBSERVER_H