#include "subject.h"
#include "observer.h"

Subject::Subject() : m_observers(std::vector<Observer*>()) {

}

Subject::~Subject() {
	/*for (auto p : m_observers) {
	if (p) {
	delete p;
	p = nullptr;
	}
	}*/
	m_observers.clear();
}

void Subject::Attach(Observer *o) {
	m_observers.push_back(o);
}

void Subject::Detach(Observer *o) {
	std::vector<Observer*>::const_iterator itr = m_observers.cbegin();
	for (; itr != m_observers.cend(); ++itr) {
		if (*itr == o) {
			m_observers.erase(itr);
			break;//iterator invalidated.
		}
	}
}

void Subject::Notify() {
	std::vector<Observer*>::iterator itr = m_observers.begin();
	for (; itr != m_observers.end(); ++itr) {
		(*itr)->Update();
	}
}
