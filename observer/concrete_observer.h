#ifndef CONCRETE_OBSERVER_H
#define CONCRETE_OBSERVER_H

#include "observer.h"
#include "observer_state.h"
class ConcreteObserver :
	public Observer
{
public:
	ConcreteObserver(Subject *s);
	~ConcreteObserver(void);
	void Update() override;

private:
	ObserverState m_state;
};

#endif//CONCRETE_OBSERVER_H