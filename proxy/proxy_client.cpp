#include "proxy_client.h"
#include "proxy.h"
#include "proxy_delegate.h"

ProxyClient::ProxyClient(void)
{
	BuyHouseImpl bhi;
	BuyHouseProxy proxy(&bhi);
	proxy.Buy();
}


ProxyClient::~ProxyClient(void)
{
}
