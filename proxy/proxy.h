#ifndef PROXY_H
#define PROXY_H

#include "proxy_interface.h"

class BuyHouseProxy : public BuyHouse {
public:
	BuyHouseProxy(BuyHouse *buy_house);
	void Buy() override;

private:
	BuyHouse *m_buy_house_impl;
}; 

#endif//PROXY_H
