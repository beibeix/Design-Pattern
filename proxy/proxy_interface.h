#ifndef PROXY_INTERFACE_H
#define PROXY_INTERFACE_H

class BuyHouse {
public:
	virtual void Buy() = 0;
};

#endif//PROXY_INTERFACE_H
