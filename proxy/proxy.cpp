#include "proxy.h"
#include <iostream>
#include "proxy_delegate.h"
using namespace std;

BuyHouseProxy::BuyHouseProxy(BuyHouse *buy_house) 
	: m_buy_house_impl(buy_house)
{

}

void BuyHouseProxy::Buy()
{
	cout<<"pre shouxu"<<endl;
	m_buy_house_impl->Buy();
	cout<<"post shouxu"<<endl;
}
