#ifndef PROXY_DELEGATE_H
#define PROXY_DELEGATE_H

#include "proxy_interface.h"

class BuyHouseImpl : public BuyHouse {
public:
	void Buy() override;
};
#endif//PROXY_DELEGATE_H
