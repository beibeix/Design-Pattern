cmake_minimum_required(VERSION 2.8)
project(TemplateMethod)

file(GLOB_RECURSE CURRENT_HEADERS  *.h *.hpp)
source_group("Include" FILES ${CURRENT_HEADERS})

#添加cpp或c文件路径
aux_source_directory(./ SRC_MAIN)


#输出可执行文件配置
add_executable(TemplateMethod ${SRC_MAIN} ${CURRENT_HEADERS} )
