#include "template_method.h"

void BubbleSort::DoSort() {
	if (m_length < 2)
		return;

	for (int i = 0; i < m_length - 1; i++) {
		for (int j = 0; j < m_length - i - 1; j++) {
			if (OutOfOrder(j)) 
				Swap(j);
		}
	}
}

IntBubbleSort::IntBubbleSort(int *pArr, unsigned int len) 
	: m_pdata(pArr) {
	m_length = len;
}

void IntBubbleSort::Swap(const unsigned int &index) {
	if (index < m_length - 1) {
		if (m_pdata[index] > m_pdata[index + 1]) {
			int tmp = m_pdata[index];
			m_pdata[index] = m_pdata[index+1];
			m_pdata[index+1] = tmp;
		}
	}
}

bool IntBubbleSort::OutOfOrder(const unsigned int &index) const {
	if (m_pdata[index] > m_pdata[index+1])
		return true;
	return false;
}
