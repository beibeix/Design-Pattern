#ifndef TEMPLATE_METHOD_H
#define TEMPLATE_METHOD_H
/*
*模板方法是一个算法的抽象定义，逐步的定义了该算法，
*每一步调用一个抽象操作或原语操作，子类定义抽象操作以具体实现该算法。
*/
class BubbleSort {
public:
	void DoSort();
protected:
	virtual void Swap(const unsigned int&) = 0;
	virtual bool OutOfOrder(const unsigned int&) const = 0;

protected:
	unsigned int m_length;
};

class IntBubbleSort : public BubbleSort {
public:
	IntBubbleSort(int *pArr, unsigned int len);

private:
	void Swap(const unsigned int &index) override;
	bool OutOfOrder(const unsigned int &index) const override;

private:
	int *m_pdata;
};



#endif//TEMPLATE_METHOD_H
