// main.cpp : 定义控制台应用程序的入口点。
//

#include "template_method.h"

int main()
{	
    int arr[] = {9,8,7,6,5,4};
	IntBubbleSort is(arr, 6);
	is.DoSort();
	return 0;
}

