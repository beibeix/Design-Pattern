#ifndef CLS_ADAPTER_H
#define CLS_ADAPTER_H

#pragma once
#include "adaptee.h"
#include "target.h"

class ClsAdapter : public Target,
	private Adaptee
{
public:
	ClsAdapter(void);
	~ClsAdapter(void);
	void Request() override;
};

#endif