#ifndef OBJ_ADAPTER_H
#define OBJ_ADAPTER_H

#include "target.h"
/*将一个类的接口转换成客户期望的另外一个接口。
Convert the interface of a class into another interface clients expect.
适配器模式使得原本由于接口不兼容而不能在一起工作的那些类可以在一起工作。
Adapter lets classes work together that couldn't otherwise because of incompatible interface.
*/
#pragma once
class Adaptee;
class ObjAdapter : public Target
{
public:
	ObjAdapter(void);
	~ObjAdapter(void);
	void Request() override;

private:
	Adaptee *m_adaptee;
};

#endif//OBJ_ADAPTER_H
