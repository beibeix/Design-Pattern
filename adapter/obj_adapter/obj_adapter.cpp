#include "obj_adapter.h"
#include "adaptee.h"

ObjAdapter::ObjAdapter(void) : m_adaptee(new Adaptee())
{
}


ObjAdapter::~ObjAdapter(void)
{
	if (m_adaptee)
	{
		delete m_adaptee;
		m_adaptee = nullptr;
	}
}

void ObjAdapter::Request()
{
	if (m_adaptee)
	{
		m_adaptee->SpecificRequest();
	} else {
		//todo:exception.
	}
}
