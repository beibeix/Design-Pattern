// main.cpp : 定义控制台应用程序的入口点。
//

#include "beverage.h"
#include "coffe_bean_1.h"
#include "moca.h"
#include "milk.h"

int main()
{	
	Beverage* bev = new CoffeeBean1();
	bev = new Moca(bev);
	bev = new Milk(bev);

	std::string desp = bev->GetDescription();
	double price = bev->GetPrice();

	system("pause");
	return 0;
}

