#include "decorator.h"

Decorator::Decorator() : m_beverage(nullptr)
{
	m_description = "just a decorator.";
	m_price = 0;
}

std::string Decorator::GetDescription()
{
	return m_description;
}

double Decorator::GetPrice()
{
	return m_price;
}
