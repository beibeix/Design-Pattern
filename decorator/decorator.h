#ifndef DECORATOR_H
#define DECORATOR_H

/***
**要想正确理解设计模式，首先必须明确它是为了解决什么问题而提出来的。
**** https://blog.csdn.net/zhshulin/article/details/38665187
**/

#include "beverage.h"

class Decorator : public Beverage
{
public:
	Decorator();
	std::string GetDescription() override;
	double GetPrice() override;
protected:
	Beverage *m_beverage;
};

#endif//DECORATOR_H
