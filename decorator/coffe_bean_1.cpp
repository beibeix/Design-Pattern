#include "coffe_bean_1.h"

CoffeeBean1::CoffeeBean1()
{
	m_description = "选择了第一种咖啡豆";
	m_price = 50;
}

std::string CoffeeBean1::GetDescription()
{
	return m_description;
}

double CoffeeBean1::GetPrice()
{
	return m_price;
}
