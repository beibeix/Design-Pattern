#include "milk.h"

Milk::Milk(Beverage* beve)
{
	m_description = "add milk.";
	m_price = 39;
	m_beverage = beve;
}

std::string Milk::GetDescription()
{
	return m_beverage->GetDescription() + m_description;
}

double Milk::GetPrice()
{
	return m_price + m_beverage->GetPrice();
}
