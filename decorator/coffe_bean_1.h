#ifndef COFFE_BEAN_1_H
#define COFFE_BEAN_1_H
#include "beverage.h"

class CoffeeBean1 : public Beverage {
public:
	CoffeeBean1();
	std::string GetDescription() override;
	double GetPrice() override;

private:
	/*std::string m_description;
	double m_price;*/
};
#endif//COFFE_BEAN_1_H
