#include "moca.h"

Moca::Moca(Beverage* beve)
{
	m_description = "add moca";
	m_beverage = beve;
	m_price = 49;
}

std::string Moca::GetDescription()
{
	return m_beverage->GetDescription() + m_description;
}

double Moca::GetPrice()
{
	return m_price + m_beverage->GetPrice();
}
