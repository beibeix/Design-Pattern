#ifndef MOCA_H
#define MOCA_H
#include "decorator.h"

class Moca : public Decorator
{
public:
	Moca(Beverage* beve);
	std::string GetDescription() override;
	double GetPrice() override;

};
#endif//MOCA_H
