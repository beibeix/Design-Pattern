#ifndef COFFEE_BEAN_2_H
#define COFFEE_BEAN_2_H
#include "beverage.h"

class CoffeeBean2 :public Beverage
{
public:
	CoffeeBean2();
	std::string GetDescription() override;
	double GetPrice() override;
private:
	
};
#endif//COFFEE_BEAN_2_H
