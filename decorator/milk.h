#ifndef MILK_H
#define MILK_H

#include "decorator.h"

class Milk : public Decorator
{
public:
	Milk(Beverage* beve);
	std::string GetDescription() override;
	double GetPrice() override;

};

#endif//MILK_H