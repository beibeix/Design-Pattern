#ifndef BEVERAGE_H
#define BEVERAGE_H
#include <string>

class Beverage
{
public:
	virtual std::string GetDescription() = 0;
	virtual double GetPrice() = 0;

protected:
	std::string m_description;
	double m_price;
};

#endif//BEVERAGE_H
