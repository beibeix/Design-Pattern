#include "coffee_bean_2.h"

CoffeeBean2::CoffeeBean2()
{
	m_description = "选择了第二种咖啡豆";
	m_price = 100;
}

std::string CoffeeBean2::GetDescription()
{
	return m_description;
}

double CoffeeBean2::GetPrice()
{
	return m_price;
}
