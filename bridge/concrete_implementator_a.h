#pragma once
#include "implementator.h"
class ConcreteImplementatorA :
	public Implementator
{
public:
	ConcreteImplementatorA(void);
	~ConcreteImplementatorA(void);
	void OperationImp() override;
};

