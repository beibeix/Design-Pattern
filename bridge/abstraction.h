#ifndef ABSTRACT_H
#define ABSTRACT_H
/*
将抽象的部分和实现部分分离，使它们可以独立的变化。
*/
class Implementator;
class Abstraction {
public:
	Abstraction();
	~Abstraction();
	virtual void Operation();
	void SetImplementator(Implementator *imp);

protected:
	Implementator *m_imp;
};
#endif//ABSTRACT_H
