#pragma once
#include "implementator.h"
class ConcreteImplementatorB :
	public Implementator
{
public:
	ConcreteImplementatorB(void);
	~ConcreteImplementatorB(void);
	void OperationImp() override;
};

