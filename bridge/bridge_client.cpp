#include "bridge_client.h"
#include "refined_abstraction.h"
#include "concrete_implementator_a.h"
#include "concrete_implementator_b.h"

BridgeClient::BridgeClient(void)
{
	RefinedAbstraction ra;
	ra.Operation();

	ra.SetImplementator(new ConcreteImplementatorA);
	ra.Operation();

	ra.SetImplementator(new ConcreteImplementatorB);
	ra.Operation();
}


BridgeClient::~BridgeClient(void)
{
}
