#ifndef REFINED_ABSTRACTION_H
#define REFINED_ABSTRACTION_H

#pragma once
#include "abstraction.h"
class RefinedAbstraction :
	public Abstraction
{
public:
	RefinedAbstraction(void);
	~RefinedAbstraction(void);
	void Operation() override;
};

#endif//REFINED_ABSTRACTION_H