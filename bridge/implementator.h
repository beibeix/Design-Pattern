#ifndef IMPLEMENTATOR_H
#define IMPLEMENTATOR_H

#pragma once

class Implementator
{
public:
	Implementator(void);
	~Implementator(void);
	virtual void OperationImp();
};
#endif//IMPLEMENTATOR_H
