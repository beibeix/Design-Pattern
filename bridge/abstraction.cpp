#include "abstraction.h"
#include "implementator.h"

Abstraction::Abstraction() : m_imp(new Implementator)
{

}

Abstraction::~Abstraction()
{
	if (m_imp)
	{
		delete m_imp;
		m_imp = nullptr;
	}
}

void Abstraction::Operation()
{
	if (m_imp)
	{
		m_imp->OperationImp();
	} else {
		//TODO:EXCEPTION.
	}
}

void Abstraction::SetImplementator(Implementator *imp)
{
	if (m_imp)
	{
		delete m_imp;
	} 
	m_imp = imp;
}
