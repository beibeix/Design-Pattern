#ifndef COMPONENT_H
#define COMPONENT_H
/*
*将对象组合成树形结构以表示“部分-整体”的层次结构。
*组合模式使得用户对单个对象和组合对象的使用具有一致性。
*/
#include <iostream>

class Component {
public:
	virtual void Operation() = 0;
	virtual void Add(Component *) { }
	virtual void Remove(Component *) { }
	virtual Component* GetChild(unsigned int) { return nullptr; }

protected:
	unsigned int m_index;
	Component *m_father;
};

#endif//COMPONENT_H
