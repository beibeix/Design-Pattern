#include "composite_client.h"
#include "leaf.h"
#include "composite.h"
#include <random>

CompositeClient::CompositeClient(void)
{
	Composite c;
	for (unsigned i = 1; i < 5; i++)
	{
		c.Add(new Leaf(i, &c));
	}
	
	c.Operation();
	
	std::uniform_int_distribution<unsigned int> index(1, 4);
	std::default_random_engine dre;
	for (int i = 0; i < 20; ++i) 
	{
		unsigned int component_index = index(dre);
		Component *comp = c.GetChild(component_index);
		if (comp)
		{
			comp->Operation();
		}
	}	
}


CompositeClient::~CompositeClient(void)
{
}
