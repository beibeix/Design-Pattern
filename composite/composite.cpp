#include "composite.h"

Composite::Composite(unsigned int index, Component *parent)
{
	m_index = index;
	m_father = parent;
}


Composite::~Composite(void)
{
	Itr itr = m_children.begin();
	for (; itr != m_children.end(); itr++)
	{
		delete *itr;
		*itr = nullptr;
	}
	m_children.clear();
}

void Composite::Operation()
{
	std::cout << "composite node, index = " << m_index << std::endl;
	Itr itr = m_children.begin();
	for (; itr != m_children.end(); itr++)
	{
		(*itr)->Operation();//成员访问运算符优先级高于解引用运算符
	}
}

void Composite::Add(Component *c)
{
	m_children.push_back(c);
}

void Composite::Remove(Component *c)
{
	CItr itr = m_children.cbegin();
	for (; itr != m_children.cend(); itr++)
	{
		if (c == *itr)
		{
			m_children.erase(itr);
			break;
		}
	}
}

Component* Composite::GetChild(unsigned int index)
{
	Component* c = nullptr;
	if (index < m_children.size())
	{
		c = m_children[index];		
	}
	else
	{
		//TODO:exception.
	}
	return c;
}
