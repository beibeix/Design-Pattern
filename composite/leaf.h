#ifndef LEAF_H
#define LEAF_H

#pragma once
#include "component.h"

class Leaf :
	public Component
{
public:
	Leaf(unsigned int index = 0, Component *parent = nullptr);
	~Leaf(void);
	void Operation() override;
};

#endif//LEAF_H