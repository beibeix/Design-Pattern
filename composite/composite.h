#ifndef COMPOSITE_H
#define COMPOSITE_H

#pragma once
#include "component.h"
#include <vector>

class Composite :
	public Component
{
	typedef std::vector<Component*>::const_iterator CItr;
	typedef std::vector<Component*>::iterator Itr;
public:
	Composite(unsigned int index = 0, Component *parent = nullptr);
	~Composite(void);
	void Operation() override;
	void Add(Component *c) override;
	void Remove(Component *c) override;
	Component* GetChild(unsigned int index) override;

private:
	std::vector<Component*> m_children;
};

#endif//COMPOSITE_H