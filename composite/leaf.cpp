#include "leaf.h"

Leaf::Leaf(unsigned int index, Component *parent)
{
	m_index = index;
	m_father = parent;
}


Leaf::~Leaf(void)
{
}

void Leaf::Operation()
{
	std::cout << "Leaf node, index = " << m_index << std::endl;
}
