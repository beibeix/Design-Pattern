#include "concrete_creator.h"
#include "product.h"
#include "concrete_product.h"

ConcreteCreator::ConcreteCreator(void)
{
}

ConcreteCreator::~ConcreteCreator(void)
{
}

Product* ConcreteCreator::FactoryMethod()
{
	return new ConcreteProduct();
}
