#ifndef CONCRETE_METHOD_H
#define CONCRETE_METHOD_H

#include "creator.h"
class ConcreteCreator :
	public Creator
{
public:
	ConcreteCreator(void);
	~ConcreteCreator(void);
	Product* FactoryMethod() override;
};
#endif//CONCRETE_METHOD_H
