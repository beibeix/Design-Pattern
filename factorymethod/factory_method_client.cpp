#include "factory_method_client.h"
#include "concrete_creator.h"

FactoryMethodClient::FactoryMethodClient(void)
{
	ConcreteCreator cc;
	Product* p = cc.FactoryMethod();
}

FactoryMethodClient::~FactoryMethodClient(void)
{
}
