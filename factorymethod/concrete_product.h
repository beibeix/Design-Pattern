#ifndef CONCRETE_PRODUCT_H
#define CONCRETE_PRODUCT_H
#include "product.h"

class ConcreteProduct :
	public Product
{
public:
	ConcreteProduct(void);
	~ConcreteProduct(void);
};
#endif//CONCRETE_PRODUCT_H
