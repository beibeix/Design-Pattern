#ifndef FACTORY_METHOD_H_H
#define FACTORY_METHOD_H_H
/************************************************************************/
/*             工厂方法模式                                              */
/*定义一个用于创建对象的接口，让子类决定实例化哪一个类。                    */
/*工厂方法模式使一个类的实例化延迟到子类。                                 */
/************************************************************************/
class Product;
class Creator {
public:
	virtual Product* FactoryMethod() = 0;
};
#endif//FACTORY_METHOD_H_H
