#ifndef INVOKER_H
#define INVOKER_H

#include <list>
#include <algorithm>
using namespace std;

//实现多次撤销重做的原理：
/*1、维护undo和redo两个盛放Command的栈（用List实现），
  2、首次执行一个Command时，执行execute()并将其放入undo栈内，同时要清空redo栈；
  3、当执行撤销操作时把undo栈内最上面一个Command拿出来执行undo()，然后将其放入redo栈内；
  4、执行重做操作时把redo栈内最上面一个Command拿出来执行execute()，然后将其放入undo栈内。
/*--------------------- 
	作者：sjepy 
	来源：CSDN 
原文：https://blog.csdn.net/sjepy/article/details/7628310 
版权声明：本文为博主原创文章，转载请附上博文链接！
*/
class Command;

class Invoker
{
public:
	Invoker();
	~Invoker(void);
	void SetCommand(Command* cmd);
	void RunCommand();
	void Undo();
	void Redo();

private:
	Command *m_cmd;
	list<Command*> m_undo_cmds;
	list<Command*> m_redo_cmds;
	int m_undo_count;
};

#endif//INVOKER_H