#include "concrete_command.h"
#include "receiver.h"

ConcreteCommand::ConcreteCommand(MyReceiver* receiver) 
	: m_receiver(receiver)
{
}


ConcreteCommand::~ConcreteCommand(void) {
	if (nullptr != m_receiver) {
		delete m_receiver;
		m_receiver = nullptr;
	}
}

void ConcreteCommand::Execute() {
	m_receiver->Act();
}

void ConcreteCommand::Undo()
{

}

void ConcreteCommand::Redo()
{

}
