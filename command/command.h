#ifndef COMMAND_H
#define COMMAND_H

class Command
{
public:
	virtual ~Command(void){};
	virtual void Execute()=0;
	virtual void Undo()=0;
	virtual void Redo()=0;

protected:
	Command(void){};
};

#endif//COMMAND_H