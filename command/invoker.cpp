#include "invoker.h"
#include "command.h"

Invoker::Invoker() : m_undo_count(-1) {
	m_undo_count = 5;
}

Invoker::~Invoker(void)
{
}

void Invoker::SetCommand(Command* cmd) {
	m_cmd = cmd;
}

void Invoker::RunCommand() {
	// 执行操作
	m_cmd->Execute();

	// m_undo_cmds.add(m_cmd);
	m_undo_cmds.push_front(m_cmd);

	// 保留最近undoCount次操作，删除最早操作
	if (m_undo_count != -1 && m_undo_cmds.size() > m_undo_count) {
		m_undo_cmds.pop_back();
	}

	// 执行新操作后清空redoList，因为这些操作不能恢复了
	m_redo_cmds.clear();
}

void Invoker::Undo()
{
	if (m_undo_cmds.size() <= 0) {
		return;
	}

	Command* cmd = ((Command*)(m_undo_cmds.front()));
	cmd->Undo();

	//m_undo_cmds.remove(cmd);
	m_undo_cmds.pop_front();
	//m_redo_cmds.add(cmd);
	m_redo_cmds.push_front(cmd);
}

void Invoker::Redo() {
	if (m_redo_cmds.size() <= 0) {
		return;
	}

	Command* cmd = ((Command*)(m_redo_cmds.front()));
	cmd->Execute();

	//m_redo_cmds.remove(cmd);
	m_redo_cmds.pop_front();
	//m_undo_cmds.add(cmd);
	m_undo_cmds.push_front(cmd);
}

