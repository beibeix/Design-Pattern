#pragma once
#include "command.h"
class MyReceiver;
class ConcreteCommand : public Command
{
public:
	ConcreteCommand(MyReceiver* receiver);
	~ConcreteCommand(void);
	void Execute() override;
	void Undo() override;
	void Redo() override;
private:
	MyReceiver *m_receiver;
};

