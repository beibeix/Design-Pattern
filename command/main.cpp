// main.cpp : 定义控制台应用程序的入口点。
//

#include "receiver.h"
#include "simple_command.h"
#include "concrete_command.h"
#include "invoker.h"


int main()
{
	
	MyReceiver* my_recv = new MyReceiver();

	Command* cmd = new SimpleCommand<MyReceiver>(my_recv, &MyReceiver::Act);
	cmd->Execute();

	/*ConcreteCommand *cc = new ConcreteCommand(my_recv);
	Invoker *invoker = new Invoker();
	invoker->SetCommand(cc);
	invoker->RunCommand();
	invoker->Undo();
	invoker->Redo();*/

	system("pause");
	return 0;
}

