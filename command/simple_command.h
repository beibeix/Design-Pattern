#ifndef SIMPLE_COMMAND_H
#define SIMPLE_COMMAND_H
#include "command.h"

template<class Receiver>
class SimpleCommand : public Command {
public:
	typedef void (Receiver::* Action)();//指向成员函数的指针
	SimpleCommand(Receiver* r, Action a)
		:m_receiver(r), m_action(a) { }

	virtual void Execute();
	void Undo() override;
	void Redo() override;

private:
	Action m_action;
	Receiver* m_receiver;
};

template<class Receiver>
void SimpleCommand<Receiver>::Redo()
{

}

template<class Receiver>
void SimpleCommand<Receiver>::Undo()
{

}

template<class Receiver>
void SimpleCommand<Receiver>::Execute() {
	/*m_receiver->Act();*/
	(m_receiver->*m_action)();
}

#endif//SIMPLE_COMMAND_H
