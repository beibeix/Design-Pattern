#include "concrete_builder.h"
#include <iostream>

ConcreteBuilder::ConcreteBuilder(void)
{
}


ConcreteBuilder::~ConcreteBuilder(void)
{
}

void ConcreteBuilder::CreatePartA()
{
	std::cout<<"create part A" <<std::endl;
}

void ConcreteBuilder::CreatePartB()
{
	std::cout<<"create part B" <<std::endl;
}

void ConcreteBuilder::CreatePartC()
{
	std::cout<<"create part C" <<std::endl;
}
