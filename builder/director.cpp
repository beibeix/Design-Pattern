#include "director.h"
#include "builder.h"
#include <iostream>

Director::Director(Builder *builder)
	: m_builder(builder)
{

}

Director::~Director(void)
{

}

void Director::Construct()
{
	//��������
	m_builder->CreatePartA();
	m_builder->CreatePartB();
	m_builder->CreatePartC();
}

void Director::GetResult() const
{
	std::cout<<"GetResult"<<std::endl;
}
