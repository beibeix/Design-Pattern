#pragma once
class Builder;
class Director
{
public:
	explicit Director(Builder *builder);
	~Director(void);
	void Construct();

	void GetResult() const;

private:
	Builder *m_builder;
};

