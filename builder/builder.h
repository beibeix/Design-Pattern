#ifndef BUILDER_H
#define BUILDER_H
/************************************************************************/
/*                         生成器模式                                    */
/* 将一个复杂对象的构建与它的表示分离，使得同样的构建过程可以创建不同的表示  */
/*
*生成复杂对象，生成对象的算法与表示过程分离.
*/
/************************************************************************/
class Builder {
public:
	virtual void CreatePartA() = 0;
	virtual void CreatePartB() = 0;
	virtual void CreatePartC() = 0;
};

#endif//BUILDER_H
