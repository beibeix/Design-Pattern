#pragma once
#include "builder.h"
class ConcreteBuilder :
	public Builder
{
public:
	ConcreteBuilder(void);
	~ConcreteBuilder(void);

	void CreatePartA() override;
	void CreatePartB() override;
	void CreatePartC() override;
};

