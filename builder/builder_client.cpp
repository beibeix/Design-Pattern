#include "builder_client.h"
#include "concrete_builder.h"
#include "director.h"

BuilderClient::BuilderClient(void)
{
	ConcreteBuilder cb;
	Director d(&cb);
	d.Construct();
	d.GetResult();
}


BuilderClient::~BuilderClient(void)
{
}
